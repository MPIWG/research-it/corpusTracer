# CorpusTracer


CorpusTracer is a platform used to create and access the database of the [Sphaera](http://sphaera.mpiwg-berlin.mpg.de) project. It is based on [Metaphactory](http://www.metaphacts.com) and [ResearchSpace](http://www.researchspace.org).

The Sphaera project is led by [Matteo Valleriani](http://www.mpiwg-berlin.mpg.de/en/users/valleriani). CorpusTracer was developed by [Florian Kräutli](http://www.mpiwg-berlin.mpg.de/en/users/fkraeutli).

### About the database

The *Sphaera* project examines the publication history surrounding one text: the tractatus *De Sphaera* by Johannes de Sacrobosco. The data that we gather in the project consists of the bibliographic data of a corpus of currently 320 books. For each book, we identify the texts that appear in the book. For each text, we capture whether it is an *original* or an adaption of an existing text (e.g. translation, annotated version, etc.).

The basic entities of the database are therefore:

- Books
- Parts
- Part Adaptions
- Persons (who can be authors, translators, publishers or printers)
- Places

The data is modelled according to the [CIDOC-CRM](http://www.cidoc-crm.org) formal ontology and the [FRBRoo](https://www.ifla.org/publications/node/11240) extnsion for bibliographic records. We make use of the [Erlangen](http://erlangen-crm.org/) RDF implementation of CIDOC-CRM and FRBRoo.

### What you can do with this code

You can use this platform to import and edit the [data that we published](https://dataverse.mpiwg-berlin.mpg.de/dataset.xhtml?persistentId=doi:10.5072/FK2/ADTPMR) and which is accessible in read-only format on a [public instance](https://db.sphaera.mpiwg-berlin.mpg.de) of the platform.

You can use the platform to contribute to our data by extending our corpus. [Get in touch with us](https://www.mpiwg-berlin.mpg.de/users/valleriani) for the best way of doing this.

Finally, you can use this platform to create a dataset for your own corpus history model. If you're happy with our model, no adjustments are necessary. However, the data generated will use our naming scheme (until we find the time to generalise it).



## How to run

### Prerequisites

You need to have [Docker](https://www.docker.com/) installed on your computer. You can find Docker for Mac [here](https://docs.docker.com/docker-for-mac/install/#download-docker-for-mac).

**Important**:
In order to access the Docker images, you need to obtain the login credentials via http://metaphacts.com/corpustracer

Once you retrieve your login details, log in to the docker registry by opening a terminal window and entering

```
docker login docker.metaphacts.com
```

### Running the platform

Download the code, either by clicking the download button on the top right or (preferably) by opening your terminal and entering:

```
git clone https://gitlab.gwdg.de/MPIWG/research-it/corpusTracer.git
```
This will create a new directory *corpusTracer* and download the code. Navigate to the directory with

```
cd corpusTracer
```

Start the platform by entering

```
docker-compose up -d
```

Finally, navigate to [http://localhost:8888](http://localhost:8888). You should see the metaphactory platform and be able to login.

| **Username:** | admin |
| ---	| --- |
| **Password**: | **admin** |

#### Stop and restart

To stop, execute in the Terminal
```
docker-compose stop
```

To restart, type
```
docker-compose restart
```

#### Deleting

To delete the containers, type
```
docker-compose down
```

Careful! The data volumes will be created fresh when running it again, meaning data created or changed in the platform will be lost.

## Loading data

You can load the Sphaera dataset into your instance of CorpusTracer. This can be useful if you want to use and query the dataset locally, or if you want to add your own data to it.

To do so:

1. Navigate to the dataset on the MPIWG Dataverse: https://dataverse.mpiwg-berlin.mpg.de/dataset.xhtml?persistentId=doi:10.5072/FK2/ADTPMR
2. Download the Sphaera Database in **N-Quads** format (be sure to pick the right format)
3. CorpusTracer has an import functionality, which you find by clicking on the cog wheel in the top right and choosing Data Import & Export. However, as the file we just downloaded is rather large, it has to be ingested directly into the Blazegraph database. You can access your Blazegraph instance by navigating to http://localhost:8888/blazegraph.
4. Click on the Update tab
5. Click Choose File and pick the Sphaera Database dump downloaded before
6. Under Type choose RDF-Data
7. Under Format choose N-Quads
8. Click Update
9. Wait for the udpdate to finish. If you don't see an error message, head back to http://localhost:8888. You should now see the data
