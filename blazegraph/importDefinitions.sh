#!/bin/bash

FOLDER=/definitions

urlencode() {
    # urlencode <string>
    old_lc_collate=$LC_COLLATE
    LC_COLLATE=C

    local length="${#1}"
    for (( i = 0; i < length; i++ )); do
        local c="${1:i:1}"
        case $c in
            [a-zA-Z0-9.~_-]) printf "$c" ;;
            *) printf '%%%02X' "'$c" ;;
        esac
    done

    LC_COLLATE=$old_lc_collate
}

urldecode() {
    # urldecode <string>

    local url_encoded="${1//+/ }"
    printf '%b' "${url_encoded//%/\\x}"
}


LOGFILE=/importLog.log

touch $LOGFILE
echo "" > $LOGFILE

cd $FOLDER || exit
for f in *.ttl ; do
  GRAPH=$(urldecode $f | sed -e 's/\.ttl//g')
  QUERY="INSERT DATA { GRAPH <$GRAPH> { $(cat $f) } }"
  echo "-------------- $f" >> $LOGFILE
  curl -X POST http://localhost:8080/blazegraph/sparql --data-urlencode "update=$QUERY" >> $LOGFILE
done
