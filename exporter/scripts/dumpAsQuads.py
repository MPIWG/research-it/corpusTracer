import urllib
from pymantic import sparql

server = sparql.SPARQLServer('http://blazegraph:8080/blazegraph/sparql')
queries = {
    'graphQuery' : "SELECT DISTINCT ?graph WHERE {GRAPH ?graph { } }",
    'graphTriplesQuery' : "SELECT ?s ?p ?o WHERE { GRAPH <%s> { ?s ?p ?o } } ORDER BY STR(?s) STR(?p) STR(?o)"
}

fields = server.query(queries['graphQuery'])

for field in fields['results']['bindings']:
    graph = str(field['graph']['value'])
    triples = server.query(queries['graphTriplesQuery'] % graph)
    for triple in triples['results']['bindings']:
        output = ''
        output += "<%s> <%s> " % (triple['s']['value'].encode('utf-8'), triple['p']['value'].encode('utf-8'))
        if ( triple['o']['type'] == 'uri') :
            output += "<%s> " % triple['o']['value'].encode('utf-8')
        else :
            output += "\"%s\" " % triple['o']['value'].encode('utf-8').replace('\n',"\\n").replace('"', '\\"')
        output += "<%s> . " % graph
        print(output)
