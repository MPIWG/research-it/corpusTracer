import urllib
from pymantic import sparql

outputFolder = '/export'

server = sparql.SPARQLServer('http://blazegraph:8080/blazegraph/sparql')
queries = {
    'graphQuery' : "PREFIX sphType: <http://sphaera.mpiwg-berlin.mpg.de/id/thes/type/> PREFIX skos: <http://www.w3.org/2004/02/skos/core#> PREFIX fields: <http://www.metaphacts.com/ontology/fields#> PREFIX ecrm: <http://erlangen-crm.org/current/> PREFIX : <http://sphaera.mpiwg-berlin.mpg.de/resource/> PREFIX sphRole: <http://sphaera.mpiwg-berlin.mpg.de/id/thes/role/> SELECT DISTINCT ?graph WHERE { { GRAPH ?graph { ?field a fields:Field } } UNION { GRAPH ?graph { ?term a ecrm:E55_Type ; skos:broader <http://sphaera.mpiwg-berlin.mpg.de/id/thes/role> } } UNION { GRAPH ?graph { ?term a ecrm:E55_Type . <http://sphaera.mpiwg-berlin.mpg.de/id/thes/type/partType> skos:member ?term } } UNION { GRAPH ?graph { <http://sphaera.mpiwg-berlin.mpg.de/id/thes/type/source> a ecrm:E55_Type } } }",
    'graphTriplesQuery' : "SELECT ?s ?p ?o WHERE { GRAPH <%s> { ?s ?p ?o } } ORDER BY STR(?s) STR(?p) STR(?o)"
}

fields = server.query(queries['graphQuery'])

for field in fields['results']['bindings']:
    graph = str(field['graph']['value'])
    triples = server.query(queries['graphTriplesQuery'] % graph)
    output = ''
    for triple in triples['results']['bindings']:
        output += "<%s> <%s> " % (triple['s']['value'].encode('utf-8'), triple['p']['value'].encode('utf-8'))
        if ( triple['o']['type'] == 'uri') :
            output += "<%s> .\n" % triple['o']['value'].encode('utf-8')
        else :
            output += "\"%s\" .\n" % triple['o']['value'].encode('utf-8').replace('\n',"\\n").replace('"', '\\"')


    f = open(outputFolder + '/' + urllib.quote_plus(graph) + '.ttl', 'w')
    f.write(output)
