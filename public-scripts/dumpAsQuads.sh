#!/bin/bash

# dumps content of entire database as n-quads

docker container exec sphaera_corpustracer_exporter /bin/sh -c "python /scripts/dumpAsQuads.py > dbdump.nq"
